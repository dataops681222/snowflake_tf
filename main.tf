# Création des Warehouses
resource "snowflake_warehouse" "INGESTION" {
  name            = "INGESTION_WAREHOUSE"
  warehouse_size            = "XSMALL"
  auto_suspend    = 300
  auto_resume     = true
  initially_suspended = true
}

resource "snowflake_warehouse" "new_warehouse" {
  name            = "NEW_WAREHOUSE"
  warehouse_size  = "XSMALL"
  auto_suspend    = 300
  auto_resume     = true
  initially_suspended = true
}


resource "snowflake_warehouse" "TRAITEMENT" {
  name            = "TRAITEMENT_WAREHOUSE"
  warehouse_size            = "XSMALL"
  auto_suspend    = 300
  auto_resume     = true
  initially_suspended = true
}

# Création des Users
resource "snowflake_user" "INGESTION_USER" {
  name           = "INGESTION_USER"
  default_warehouse = snowflake_warehouse.INGESTION.name
  default_role   = "INGESTION_ROLE"
  password       = "123456789"
}

resource "snowflake_user" "TRAITEMENT_USER" {
  name           = "TRAITEMENT_USER"
  default_warehouse = snowflake_warehouse.TRAITEMENT.name
  default_role   = "TRAITEMENT_ROLE"
  password       = "123456789"
}

# Création des Roles
resource "snowflake_role" "INGESTION_ROLE" {
  name    = "INGESTION_ROLE"
  comment = "Role for ingestion processes"
}

resource "snowflake_role" "TRAITEMENT_ROLE" {
  name    = "TRAITEMENT_ROLE"
  comment = "Role for traitement processes"
}

# Assignation des rôles aux utilisateurs
resource "snowflake_role_grants" "INGESTION_ROLE_GRANTS" {
  role_name = snowflake_role.INGESTION_ROLE.name

  users = [
    snowflake_user.INGESTION_USER.name,
  ]
}

resource "snowflake_role_grants" "TRAITEMENT_ROLE_GRANTS" {
  role_name = snowflake_role.TRAITEMENT_ROLE.name

  users = [
    snowflake_user.TRAITEMENT_USER.name,
  ]
}

# Création des Bases de Données
resource "snowflake_database" "BRONZE" {
  name = "BRONZE"
}

resource "snowflake_database" "SILVER" {
  name = "SILVER"
}

# Création des Schémas
resource "snowflake_schema" "AIRBYTE" {
  name       = "AIRBYTE"
  database   = snowflake_database.BRONZE.name
}

resource "snowflake_schema" "DBT" {
  name       = "DBT"
  database   = snowflake_database.SILVER.name
}

# Assignation des droits aux rôles sur les bases de données
resource "snowflake_database_grant" "BRONZE_DB_GRANT" {
  database_name = snowflake_database.BRONZE.name
  privilege     = "ALL PRIVILEGES"
  roles         = [snowflake_role.INGESTION_ROLE.name]
}

resource "snowflake_database_grant" "SILVER_DB_GRANT" {
  database_name = snowflake_database.SILVER.name
  privilege     = "ALL PRIVILEGES"
  roles         = [snowflake_role.TRAITEMENT_ROLE.name]
}

# Assignation des droits aux rôles sur les schémas
resource "snowflake_schema_grant" "AIRBYTE_SCHEMA_GRANT" {
  database_name = snowflake_database.BRONZE.name
  schema_name   = snowflake_schema.AIRBYTE.name
  privilege     = "ALL PRIVILEGES"
  roles         = [snowflake_role.INGESTION_ROLE.name]
}

resource "snowflake_schema_grant" "DBT_SCHEMA_GRANT" {
  database_name = snowflake_database.SILVER.name
  schema_name   = snowflake_schema.DBT.name
  privilege     = "ALL PRIVILEGES"
  roles         = [snowflake_role.TRAITEMENT_ROLE.name]
}