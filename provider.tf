terraform {
 required_providers {
  snowflake = {
   source = "Snowflake-Labs/snowflake"
   version = "0.82.0"
  }
 }
  backend "http" {
 }
}
provider "snowflake" {
username = var.snowflake_username
password = var.snowflake_password
account  = var.snowflake_account
role = "ACCOUNTADMIN"
}


